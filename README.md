# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do generatora zegara Si5351A-B-GT (https://www.skyworksinc.com/en/Products/Timing/CMOS-Clock-Generators/Si5351A-B-GT).

# Projekt PCB

Schemat: [doc/SI5351A_EVB_V1_0_SCH.pdf](doc/SI5351A_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/SI5351A_EVB_V1_0_3D.pdf](doc/SI5351A_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
